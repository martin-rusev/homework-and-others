package com.telerikacademy.hms.users;

import com.telerikacademy.hms.billing.HealthInsurancePlan;

public class Patient extends User {
    private static final String INSURED_ERROR_MESSAGE = "Patient already insured";
    private HealthInsurancePlan healthInsurancePlan;

    public Patient() {
        System.out.println("Default Patient constructor");
    }

    public Patient(long id, String firstName, String lastName, String gender, String email) {
        super(id, firstName, lastName, gender, email);

        System.out.println("Patient constructor with parameters");
    }

    public HealthInsurancePlan getHealthInsurancePlan() {
        return healthInsurancePlan;
    }

    public void addInsurancePlan(HealthInsurancePlan healthInsurancePlan) {
        if (!isInsured()) {
            this.healthInsurancePlan = healthInsurancePlan;
            super.addInsurancePlan(healthInsurancePlan);
        } else {
            throw new IllegalArgumentException(INSURED_ERROR_MESSAGE);
        }
    }
}
