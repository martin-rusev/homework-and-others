package com.telerikacademy.hms.users;

import com.telerikacademy.hms.billing.InsurancePlan;

import java.util.ArrayList;
import java.util.List;

public class User {
    private long id;
    private String firstName;
    private String lastName;
    private String gender;
    private String email;
    private List<InsurancePlan> insurancePlans;

    public User(long id, String firstName, String lastName, String gender, String email) {
        setId(id);
        setFirstName(firstName);
        setLastName(lastName);
        setGender(gender);
        setEmail(email);
        insurancePlans= new ArrayList<>();

        System.out.println("User constructor with parameters");
    }

    public boolean isInsured() {
        return insurancePlans != null && insurancePlans.size() > 0;
    }

    public List<InsurancePlan> getInsurancePlans() {
        return new ArrayList<>(insurancePlans);
    }

    public void addInsurancePlan(InsurancePlan insurancePlan) {
        if (insurancePlan != null){
            insurancePlans.add(insurancePlan);
        }
    }

    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    private void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    private void setGender(String gender) {
        this.gender = gender;
    }
}
