package com.telerikacademy.hms.users;

public class Nurse extends Staff {
    private int departmentId;

    public Nurse() {
        System.out.println("Default Nurse constructor");
    }

    public Nurse(long id, String firstName, String lastName, String gender, String email, int yearsOfExperience, String description, double salary, int departmentId) {
        super(id, firstName, lastName, gender, email, yearsOfExperience, description, salary);
        setDepartmentId(departmentId);

        System.out.println("Nurse constructor with parameters");
    }

    public int getDepartmentId() {
        return departmentId;
    }

    private void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }
}
