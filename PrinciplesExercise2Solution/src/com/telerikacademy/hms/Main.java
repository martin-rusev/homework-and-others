package com.telerikacademy.hms;

import com.telerikacademy.hms.billing.*;
import com.telerikacademy.hms.users.*;

public class Main {
    public static void main(String[] args) {
        Nurse nurse = new Nurse(4, "Sara", "Kennedy", "female", "sarra@gmail.com", 5, "Head of nurses", 280.45, 4);
        Doctor doctor = new Doctor(5, "Martin", "Luther", "male", "martin@gmail.com", 10, "ER", 1500.00, "Surgeon");
        Patient patient = new Patient(3, "Patric", "Jackson", "male", "pathrik@gmail.com");
        InsuranceBrand brand = new InsuranceBrand(1, "Awesome Insurances");

        System.out.println("*** Nurse with no plan ***");
        Billing.printPayments(nurse);

        System.out.println("*** Doctor with Platinum Plan and Retirement Plan ***");
        doctor.addInsurancePlan(new PlatinumPlan(brand));
        doctor.addInsurancePlan(new RetirementPlan(10, 0.02, brand));
        Billing.printPayments(doctor);

        System.out.println("*** Patient with Gold Plan ***");
        patient.addInsurancePlan(new GoldPlan(brand));
        Billing.printPayments(patient, 1000);
    }
}