package com.telerikacademy.hms.billing;

public class SilverPlan extends HealthInsurancePlan {
    public SilverPlan(InsuranceBrand offeredBy) {
        super(0.7, 30, 0.06, offeredBy);
    }
}
