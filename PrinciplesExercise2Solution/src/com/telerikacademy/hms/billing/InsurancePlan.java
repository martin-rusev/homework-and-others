package com.telerikacademy.hms.billing;

public interface InsurancePlan {
    String getName();
    InsuranceBrand getOfferedBy();
    double getPricePercent();
    double getDiscount();
    double computeMonthlyPremium(double salary);
}
