package com.telerikacademy.hms.billing;

public class PlatinumPlan extends HealthInsurancePlan {
    public PlatinumPlan(InsuranceBrand offeredBy) {
        super(0.9, 50, 0.08, offeredBy);
    }
}
