package com.telerikacademy.hms.billing;

public abstract class InsurancePlanImpl implements InsurancePlan {
    private double discount;
    private double pricePercent;
    private InsuranceBrand offeredBy;

    public InsurancePlanImpl(double discount, double pricePercent, InsuranceBrand offeredBy) {
        setDiscount(discount);
        setOfferedBy(offeredBy);
        setPricePercent(pricePercent);
    }

    public InsuranceBrand getOfferedBy() {
        return offeredBy;
    }

    private void setOfferedBy(InsuranceBrand offeredBy) {
        this.offeredBy = offeredBy;
    }

    public double getDiscount() {
        return discount;
    }

    @Override
    public double computeMonthlyPremium(double salary) {
        return salary * getPricePercent();
    }

    private void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getPricePercent() {
        return pricePercent;
    }

    private void setPricePercent(double pricePercent) {
        this.pricePercent = pricePercent;
    }
}