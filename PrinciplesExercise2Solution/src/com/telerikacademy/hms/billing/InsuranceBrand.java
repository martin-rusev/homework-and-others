package com.telerikacademy.hms.billing;

public class InsuranceBrand {
    private long id;
    private String name;

    public InsuranceBrand(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }
    private void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    private void setName(String name) {
        this.name = name;
    }
}