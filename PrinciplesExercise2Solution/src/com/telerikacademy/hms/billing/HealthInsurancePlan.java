package com.telerikacademy.hms.billing;

public class HealthInsurancePlan extends InsurancePlanImpl {
    // Code for 'coverage' field goes here
    private double coverage;

    public HealthInsurancePlan(double coverage, double discount, double pricePercent, InsuranceBrand offeredBy) {
        super(discount, pricePercent, offeredBy);
        setCoverage(coverage);
    }

    @Override
    public String getName() {
        return "Health insurances for everyone";
    }

    public double getCoverage() {
        return coverage;
    }

    private void setCoverage(double coverage) {
        this.coverage = coverage;
    }
}
