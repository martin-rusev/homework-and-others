package com.telerikacademy.hms.billing;

public class GoldPlan extends HealthInsurancePlan {
    public GoldPlan(InsuranceBrand offeredBy) {
        super(0.8, 40, 0.07, offeredBy);
    }
}
