package com.telerikacademy.hms.billing;

public class RetirementPlan extends InsurancePlanImpl {
    public RetirementPlan(double discount, double pricePercent, InsuranceBrand offeredBy) {
        super(discount, pricePercent, offeredBy);
    }

    @Override
    public String getName() {
        return "Money for your last days";
    }
}
