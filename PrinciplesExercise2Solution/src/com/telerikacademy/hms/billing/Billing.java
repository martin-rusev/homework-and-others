package com.telerikacademy.hms.billing;

import com.telerikacademy.hms.users.Patient;
import com.telerikacademy.hms.users.Staff;

public class Billing {
    private static final String PATIENT_PAYMENT_MESSAGE_FORMAT = "Patient should pay: %.2f";
    private static final String COMPANY_PAYMENT_MESSAGE_FORMAT = "Insurance company should pay: %.2f";
    private static final String INSURANCE_PAYMENT_MESSAGE_FORMAT = "Insurance payment to be deducted from salary %.2f is: %.2f";
    private static double DEFAULT_PAYMENT_DISCOUNT = 20;

    public static double[] computePaymentAmount(Patient patient, double amount) {
        double[] payments = new double[2];
        double amountAfterDiscount = amount - DEFAULT_PAYMENT_DISCOUNT;

        // your logic
        if (patient.isInsured()) {
            HealthInsurancePlan patientInsurancePlan = patient.getHealthInsurancePlan();
            double amountAfterCoverage = amount - amount * patientInsurancePlan.getCoverage();
            amountAfterDiscount = amountAfterCoverage > patientInsurancePlan.getDiscount() ?
                    amountAfterCoverage - patientInsurancePlan.getDiscount() : 0;
        }


        payments[0] = amount - amountAfterDiscount;
        payments[1] = amountAfterDiscount;

        return payments;

    }

    public static double computeInsurancePayment(Staff staffMember) {
        double payment = 0;
        if (staffMember.isInsured()) {
            for (InsurancePlan plan : staffMember.getInsurancePlans()) {
                payment += plan.computeMonthlyPremium(staffMember.getSalary());
            }
        }

        return payment;
    }

    public static void printPayments(Patient patient, double amount) {

        double[] payments = computePaymentAmount(patient, amount);
        System.out.println(String.format(COMPANY_PAYMENT_MESSAGE_FORMAT, payments[0]));
        System.out.println(String.format(PATIENT_PAYMENT_MESSAGE_FORMAT, payments[1]));
        System.out.println();
    }

    public static void printPayments(Staff staffMember) {
        System.out.println(String.format(INSURANCE_PAYMENT_MESSAGE_FORMAT,
                staffMember.getSalary(),
                computeInsurancePayment(staffMember)));
        System.out.println();
    }
}