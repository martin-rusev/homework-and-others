package com.telerikacademy.hms.billing;

public class BronzePlan extends HealthInsurancePlan {
    public BronzePlan(InsuranceBrand offeredBy) {
        super(0.6, 25, 0.05, offeredBy);
    }
}
