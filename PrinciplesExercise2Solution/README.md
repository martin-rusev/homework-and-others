# Computing More Insurances for Staff (Part 2) ~ Let's put Interfaces into practice

In this exercise, we will re-factor our code to see what benefits comes with interfaces. It is okay that patient has health insurance plan, but staff members and users at all can have more than one insurance.

Now we are going to extend our example and add Retirement Insurance for some Users.

1. Create new interface **_InsurancePlan.java_** it should have all public methods of a common insurance plan. Let's define that each insurance plan has:
    ```java
    String getName();
    InsuranceBrand getOfferedBy();
    double getDiscount();
    double computeMonthlyPremium(double salary);
    ```
2. Now let's implement the interface. We start with common logic, defining **_InsurancePlanImpl.java_** abstract class. Remember that defining child class as abstract allows us NOT to implement some of the methods. In our case the **getName** method, as each specific insurance plan will have its own name. 
    * Create constructor with all basic fields. Note that coverage is NOT a basic field. It is specific for Health insurance plans.
    ```java
    public InsurancePlanImpl(double discount, InsuranceBrand offeredBy) {
        setDiscount(discount);
        setOfferedBy(offeredBy);
    }
    ```
    * Can we implement **computeMonthlyPremium** in this basic class? Is it the same for all plans?
    If we introduce one new field - pricePercent in the **_InsurancePlanImpl.java_**, we can calculate  **computeMonthlyPremium**  in it:
    ```java
        public double computeMonthlyPremium(double salary) {
        return salary * getPricePercent();
    }
    ```
    Don't forget to add `double getPricePercent();` in the **_InsurancePlan.java_** interface and the filed in the **InsurancePlanImpl** 's constructor;

3. It is time to create **_HealthInsurancePlan.java_** class for health insurances.
    * New fields - coverage
    * Implement  **getName** method

```java
    @Override
    public String getName() {
        return "Health insurances for everyone";
    }
```

4. Now we want all of the plans to inherit from this new Health insurance plan.

Here's the code for the Platinum Plan:
```java
public class PlatinumPlan extends HealthInsurancePlan {
    public PlatinumPlan(InsuranceBrand offeredBy) {
        super(0.9, 50, 0.08, offeredBy);
    }
}
```
Implement the rest.

5. Another type of insurance one may have is a retirement insurance. Let's create new **_RetirementPlan.java_** class.
    * It should extend InsurancePlanImpl class
    * It should implement **getName** method

6. Let's change the logic that one User may have multiple insurances.
    * Change the field `private HealthInsurancePlan insurancePlan;` to `private List<InsurancePlan> insurancePlans;`
    * Add initialization of this field in User's constructor:
    ```java
    public User(long id, String firstName, String lastName, String gender, String email) {
        setId(id);
        setFirstName(firstName);
        setLastName(lastName);
        setGender(gender);
        setEmail(email);
        insurancePlans= new ArrayList<>();

        System.out.println("User constructor with parameters");
    }
    ```
    * Re-factor getters and setters and isInsured field:
    ```java
    public List<InsurancePlan> getInsurancePlans() {
        return new ArrayList<>(insurancePlans);
    }

    public void addInsurancePlan(InsurancePlan insurancePlan) {
        if (insurancePlan != null){
            insurancePlans.add(insurancePlan);
        }
    }
    public boolean isInsured() {
        return insurancePlans != null && insurancePlans.size() > 0;
    }
    ```

7. Patients will only need Health insurance plan. To achieve this use the following code for **_Patient.java_** class:

```java
public class Patient extends User {
    private static final String INSURED_ERROR_MESSAGE = "Patient already insured";
    private HealthInsurancePlan healthInsurancePlan;

    public Patient() {
        System.out.println("Default Patient constructor");
    }

    public Patient(long id, String firstName, String lastName, String gender, String email) {
        super(id, firstName, lastName, gender, email);

        System.out.println("Patient constructor with parameters");
    }

    public HealthInsurancePlan getHealthInsurancePlan() {
        return healthInsurancePlan;
    }

    public void addInsurancePlan(HealthInsurancePlan healthInsurancePlan) {
        if (!isInsured()) {
            this.healthInsurancePlan = healthInsurancePlan;
            super.addInsurancePlan(healthInsurancePlan);
        } else {
            throw new IllegalArgumentException(INSURED_ERROR_MESSAGE);
        }
    }
}
```

7. Now re-work the **_Billing.java_** module, so it can calculate multiple insurance plans.

8. To test your code you can execute the following Main function:

```java
public static void main(String[] args) {
        Nurse nurse = new Nurse(4, "Sara", "Kennedy", "female", "sarra@gmail.com", 5, "Head of nurses", 280.45, 4);
        Doctor doctor = new Doctor(5, "Martin", "Luther", "male", "martin@gmail.com", 10, "ER", 1500.00, "Surgeon");
        Patient patient = new Patient(3, "Patric", "Jackson", "male", "pathrik@gmail.com");
        InsuranceBrand brand = new InsuranceBrand(1, "Awesome Insurances");

        System.out.println("*** Nurse with no plan ***");
        Billing.printPayments(nurse);

        System.out.println("*** Doctor with Platinum Plan and Retirement Plan ***");
        doctor.addInsurancePlan(new PlatinumPlan(brand));
        doctor.addInsurancePlan(new RetirementPlan(10, 0.02, brand));
        Billing.printPayments(doctor);

        System.out.println("*** Patient with Gold Plan ***");
        patient.addInsurancePlan(new GoldPlan(brand));
        Billing.printPayments(patient, 1000);
    }
```


Thanks & Happy Coding!!