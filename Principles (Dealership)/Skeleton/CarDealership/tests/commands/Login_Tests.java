package commands;

import com.telerikacademy.dealership.commands.Login;
import com.telerikacademy.dealership.commands.contracts.Command;
import com.telerikacademy.dealership.core.DealershipRepositoryImpl;
import com.telerikacademy.dealership.core.contracts.CommandFactory;
import com.telerikacademy.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.dealership.core.factories.CommandFactoryImpl;
import com.telerikacademy.dealership.core.factories.DealershipFactoryImpl;
import com.telerikacademy.dealership.models.UserImpl;
import com.telerikacademy.dealership.models.common.enums.Role;
import com.telerikacademy.dealership.models.contracts.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class Login_Tests {
    
    private DealershipRepository dealershipRepository;
    private DealershipFactory dealershipFactory;
    private CommandFactory commandFactory;
    
    @Before
    public void before() {
        this.commandFactory = new CommandFactoryImpl();
        this.dealershipFactory = new DealershipFactoryImpl();
        this.dealershipRepository = new DealershipRepositoryImpl();
    }
    
    @Test
    public void execute_ShouldLoginUser_WhenUserNotLoggedIn() {
        // Arrange
        User userToLogIn = new UserImpl("pesho123", "petar", "petrov", "password", Role.NORMAL);
        dealershipRepository.addUser(userToLogIn);
        Command login = new Login(dealershipFactory, dealershipRepository);
        List<String> params = new ArrayList<>();
        params.add("pesho123");
        params.add("password");
        
        // Act
        login.execute(params);
        
        // Assert
        Assert.assertEquals("pesho123", dealershipRepository.getLoggedUser().getUsername());
    }
    
    @Test
    public void execute_ShouldNotLoginUser_WhenPasswordIsWrong() {
        // Arrange
        User userToLogIn = new UserImpl("pesho123", "petar", "petrov", "password", Role.NORMAL);
        dealershipRepository.addUser(userToLogIn);
        Command login = commandFactory.createCommand("login", dealershipFactory, dealershipRepository);
        List<String> params = new ArrayList<>();
        params.add("pesho123");
        params.add("password123");
        
        // Act
        login.execute(params);
        
        // Assert
        Assert.assertNull(dealershipRepository.getLoggedUser());
    }
    
}
