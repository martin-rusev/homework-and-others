package commands;

import com.telerikacademy.dealership.commands.Login;
import com.telerikacademy.dealership.commands.contracts.Command;
import com.telerikacademy.dealership.core.DealershipRepositoryImpl;
import com.telerikacademy.dealership.core.contracts.CommandFactory;
import com.telerikacademy.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.dealership.core.factories.CommandFactoryImpl;
import com.telerikacademy.dealership.core.factories.DealershipFactoryImpl;
import com.telerikacademy.dealership.models.contracts.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static utils.Factory.*;
import static utils.Mapper.*;


public class DealershipFactoryTests {
    
    private DealershipRepository dealershipRepository;
    private DealershipFactory dealershipFactory;
    private CommandFactory commandFactory;
    
    @Before
    public void before() {
        this.commandFactory = new CommandFactoryImpl();
        this.dealershipFactory = new DealershipFactoryImpl();
        this.dealershipRepository = new DealershipRepositoryImpl();
    }
    
    @Test
    public void createCar_ShouldCreate_WhenInputIsValid() {
        logInUser(createNormalUser());
        
        // Act
        Command createVehicle = commandFactory.createCommand("AddVehicle", dealershipFactory, dealershipRepository);
        Car testCar = createCar();
        List<String> params = mapCarToParamsList(testCar);
        
        // Act
        createVehicle.execute(params);
        
        //Assert
        Car toCompare = (Car) dealershipRepository.getLoggedUser().getVehicles().get(0);
        Assert.assertEquals(testCar.getMake(), toCompare.getMake());
        Assert.assertEquals(testCar.getSeats(), toCompare.getSeats());
        
    }
    
    @Test
    public void createMotorcycle_ShouldCreate_WhenInputIsValid() {
        logInUser(createNormalUser());
        
        // Act
        Command createVehicle = commandFactory.createCommand("AddVehicle", dealershipFactory, dealershipRepository);
        Motorcycle testMotorcycle = createMotorcycle();
        List<String> params = mapMotorcycleToParamsList(testMotorcycle);
        
        // Act
        createVehicle.execute(params);
        
        //Assert
        Motorcycle toCompare = (Motorcycle) dealershipRepository.getLoggedUser().getVehicles().get(0);
        Assert.assertEquals(testMotorcycle.getMake(), toCompare.getMake());
        Assert.assertEquals(testMotorcycle.getCategory(), toCompare.getCategory());
        
    }
    
    @Test
    public void createTruck_ShouldCreate_WhenInputIsValid() {
        logInUser(createNormalUser());
        
        // Act
        Command createVehicle = commandFactory.createCommand("AddVehicle", dealershipFactory, dealershipRepository);
        Truck testTruck = createTruck();
        List<String> params = mapTruckToParamsList(testTruck);
        
        // Act
        createVehicle.execute(params);
        
        //Assert
        Truck toCompare = (Truck) dealershipRepository.getLoggedUser().getVehicles().get(0);
        Assert.assertEquals(testTruck.getMake(), toCompare.getMake());
        Assert.assertEquals(testTruck.getWeightCapacity(), toCompare.getWeightCapacity());
        
    }
    
    @Test
    public void createUser_ShouldCreate_WhenInputIsValid() {
        // Act
        Command createVehicle = commandFactory.createCommand("RegisterUser",
                dealershipFactory,
                dealershipRepository);
        User testUser = createNormalUser();
        List<String> params = mapUserToParamsList(testUser);
        
        // Act
        createVehicle.execute(params);
        
        //Assert
        User toCompare = dealershipRepository.getUsers().get(0);
        Assert.assertEquals(toCompare.getUsername(), testUser.getUsername());
    }
    
    @Test
    public void createComment_ShouldCreate_WhenInputIsValid() {
        User user = createNormalUser();
        Vehicle vehicle = (Vehicle) createCar();
        logInUser(user);
        addVehicleToUser(user, vehicle);
        
        // Act
        Command createVehicle = commandFactory.createCommand("AddComment",
                dealershipFactory,
                dealershipRepository);
        Comment testComment = createComment(user);
        List<String> params = mapCommentToParamsList(testComment);
        
        // Act
        createVehicle.execute(params);
        
        //Assert
        Comment toCompare = dealershipRepository.getLoggedUser().getVehicles().get(0).getComments().get(0);
        Assert.assertEquals(toCompare.getContent(), testComment.getContent());
        Assert.assertEquals(toCompare.getAuthor(), testComment.getAuthor());
    }
    
    private void logInUser(User userToLogIn) {
        dealershipRepository.addUser(userToLogIn);
        Command login = new Login(dealershipFactory, dealershipRepository);
        List<String> params = new ArrayList<>();
        params.add(userToLogIn.getUsername());
        params.add(userToLogIn.getPassword());
        login.execute(params);
    }
    
    private void addVehicleToUser(User user, Vehicle vehicle) {
        Command createVehicle = commandFactory.createCommand("AddVehicle",
                dealershipFactory,
                dealershipRepository);
        Car testCar = createCar();
        List<String> params = mapCarToParamsList(testCar);
        
        createVehicle.execute(params);
    }
    
}
