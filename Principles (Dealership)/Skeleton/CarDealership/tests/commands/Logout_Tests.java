package commands;

import com.telerikacademy.dealership.commands.Logout;
import com.telerikacademy.dealership.commands.contracts.Command;
import com.telerikacademy.dealership.core.DealershipRepositoryImpl;
import com.telerikacademy.dealership.core.contracts.CommandFactory;
import com.telerikacademy.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.dealership.core.factories.CommandFactoryImpl;
import com.telerikacademy.dealership.core.factories.DealershipFactoryImpl;
import com.telerikacademy.dealership.models.UserImpl;
import com.telerikacademy.dealership.models.common.enums.Role;
import com.telerikacademy.dealership.models.contracts.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class Logout_Tests {
    
    private DealershipRepository dealershipRepository;
    private DealershipFactory dealershipFactory;
    private CommandFactory commandFactory;
    
    @Before
    public void before() {
        this.commandFactory = new CommandFactoryImpl();
        this.dealershipFactory = new DealershipFactoryImpl();
        this.dealershipRepository = new DealershipRepositoryImpl();
    }
    
    @Test
    public void execute_ShouldLogoutUser() {
        // Arrange
        User userToLogIn = new UserImpl("pesho123", "petar", "petrov", "password", Role.NORMAL);
        dealershipRepository.setLoggedUser(userToLogIn);
        Command logout = new Logout(dealershipFactory, dealershipRepository);
        
        // Act
        logout.execute(new ArrayList<>());
        
        // Assert
        Assert.assertNull(dealershipRepository.getLoggedUser());
    }
    
}
