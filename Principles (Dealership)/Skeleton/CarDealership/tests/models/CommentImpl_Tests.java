package models;

import com.telerikacademy.dealership.models.CommentImpl;
import com.telerikacademy.dealership.models.contracts.Comment;
import org.junit.Assert;
import org.junit.Test;

//import static com.telerikacademy.dealership.models.common.Constants.CommentConstants.CONTENT_LEN_MAX;
//import static com.telerikacademy.dealership.models.common.Constants.CommentConstants.CONTENT_LEN_MIN;

public class CommentImpl_Tests {

    private static final int CONTENT_LEN_MIN = 3;
    private static final int CONTENT_LEN_MAX = 200;

    @Test
    public void CommentImpl_ShouldImplementCommentInterface() {
        CommentImpl comment = new CommentImpl("content", "author");
        Assert.assertTrue(comment instanceof Comment);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenContentIsNull() {
        CommentImpl comment = new CommentImpl(null, "author");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenContentIsBelow3() {
        CommentImpl comment = new CommentImpl(new String(new char[CONTENT_LEN_MIN - 1]), "author");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenContentIsAbove200() {
        CommentImpl comment = new CommentImpl(
                new String(new char[CONTENT_LEN_MAX + 1]), "author");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenAuthorIsNull() {
        CommentImpl comment = new CommentImpl(
                new String(new char[CONTENT_LEN_MIN + 2]), null);
    }
    
}
