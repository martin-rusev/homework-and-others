package models;

import com.telerikacademy.dealership.models.CarImpl;
import com.telerikacademy.dealership.models.contracts.Car;
import com.telerikacademy.dealership.models.contracts.Vehicle;
import org.junit.Assert;
import org.junit.Test;


public class CarImpl_Tests {
    
    
    @Test
    public void CarImpl_ShouldImplementCarInterface() {
        CarImpl car = new CarImpl("make", "model", 100, 4);
        // Assert
        Assert.assertTrue(car instanceof Car);
    }
    
    @Test
    public void CarImpl_ShouldImplementVehicleInterface() {
        CarImpl car = new CarImpl("make", "model", 100, 4);
        // Assert
        Assert.assertTrue(car instanceof Vehicle);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenMakeIsNull() {
        // Act
        CarImpl car = new CarImpl(null, "model", 100, 4);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenMakeLengthIsBelow2() {
        // Act
        CarImpl car = new CarImpl("m", "model", 100, 4);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenMakeLengthIsAbove15() {
        // Act
        CarImpl car = new CarImpl("makeLongCar12345", "model", 100, 4);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenModelIsNull() {
        // Act
        CarImpl car = new CarImpl("make", null, 100, 4);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenModelLengthIsBelow_1() {
        // Act
        CarImpl car = new CarImpl("make", "", 100, 4);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenModelLengthIsAbove15() {
        // Act
        CarImpl car = new CarImpl("make", "longLongModel123", 100, 4);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenPriceIsNegative() {
        // Act
        CarImpl car = new CarImpl("make", "model", -100, 4);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenPriceIsAbove1000000() {
        // Act
        CarImpl car = new CarImpl("make", "model", 1000001, 4);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenSeatsIsNegative() {
        // Act
        CarImpl car = new CarImpl("make", "model", 1000, -4);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenSeatsIsAbove10() {
        // Act
        CarImpl car = new CarImpl("make", "model", 1000, 11);
    }
    
}
