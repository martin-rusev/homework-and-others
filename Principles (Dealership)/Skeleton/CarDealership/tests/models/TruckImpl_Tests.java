package models;

import com.telerikacademy.dealership.models.TruckImpl;
import com.telerikacademy.dealership.models.contracts.Truck;
import com.telerikacademy.dealership.models.contracts.Vehicle;
import org.junit.Assert;
import org.junit.Test;

//import static com.telerikacademy.dealership.models.common.Constants.VehicleConstants.*;

public class TruckImpl_Tests {

    private static final int MAKE_NAME_LEN_MIN = 2;
    private static final int MAKE_NAME_LEN_MAX = 15;
    private static final int MODEL_NAME_LEN_MIN = 2;
    private static final int MODEL_NAME_LEN_MAX = 15;

    @Test
    public void TruckImpl_ShouldImplementTruckInterface() {
        TruckImpl truck = new TruckImpl("make", "model", 100, 10);
        Assert.assertTrue(truck instanceof Truck);
    }
    
    @Test
    public void TruckImpl_ShouldImplementVehicleInterface() {
        TruckImpl truck = new TruckImpl("make", "model", 100, 10);
        Assert.assertTrue(truck instanceof Vehicle);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenMakeIsNull() {
        TruckImpl truck = new TruckImpl(null, "model", 100, 10);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenModelIsNull() {
        TruckImpl truck = new TruckImpl("make", null, 100, 10);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenMakeLengthIsBelow2() {
        TruckImpl truck = new TruckImpl(new String(new char[MAKE_NAME_LEN_MIN - 1]), "model", 100, 10);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenMakeLengthIsAbove15() {
        TruckImpl truck = new TruckImpl(new String(new char[MAKE_NAME_LEN_MAX + 1]), "model", 100, 10);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenModelLengthIsBelow_1() {
        TruckImpl truck = new TruckImpl("make", new String(new char[MODEL_NAME_LEN_MIN - 1]), 100, 10);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenModelLengthIsAbove15() {
        TruckImpl truck = new TruckImpl("make", new String(new char[MODEL_NAME_LEN_MAX + 1]), 100, 10);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenPriceIsNegative() {
        TruckImpl truck = new TruckImpl("make", "model", -100, 10);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenPriceIsAbove1000000() {
        TruckImpl truck = new TruckImpl("make", "model", 1000001, 10);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenWeightCapacityIsNegative() {
        TruckImpl truck = new TruckImpl("make", "model", 100, -10);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenWeightCapacityIsAbove100() {
        TruckImpl truck = new TruckImpl("make", "model", 100, 101);
    }
    
}
