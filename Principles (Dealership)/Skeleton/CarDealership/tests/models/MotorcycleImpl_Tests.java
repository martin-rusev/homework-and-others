package models;

import com.telerikacademy.dealership.models.MotorcycleImpl;
import com.telerikacademy.dealership.models.contracts.Motorcycle;
import com.telerikacademy.dealership.models.contracts.Vehicle;
import org.junit.Assert;
import org.junit.Test;

//import static com.telerikacademy.dealership.models.common.Constants.MotorcycleConstants.CATEGORY_LEN_MAX;
//import static com.telerikacademy.dealership.models.common.Constants.MotorcycleConstants.CATEGORY_LEN_MIN;
//import static com.telerikacademy.dealership.models.common.Constants.VehicleConstants.*;

public class MotorcycleImpl_Tests {

    private static final int MAKE_NAME_LEN_MIN = 2;
    private static final int MAKE_NAME_LEN_MAX = 15;
    private static final int MODEL_NAME_LEN_MIN = 2;
    private static final int MODEL_NAME_LEN_MAX = 15;
    private static final int CATEGORY_LEN_MIN = 3;
    private static final int CATEGORY_LEN_MAX = 10;

    @Test
    public void MotorcycleImpl_ShouldImplementMotorcycleInterface() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 100, "category");
        Assert.assertTrue(motorcycle instanceof Motorcycle);
    }
    
    @Test
    public void MotorcycleImpl_ShouldImplementVehicleInterface() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 100, "category");
        Assert.assertTrue(motorcycle instanceof Vehicle);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenMakeIsNull() {
        MotorcycleImpl motorcycle = new MotorcycleImpl(null, "model", 100, "category");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenMakeLengthIsBelow2() {
        MotorcycleImpl motorcycle = new MotorcycleImpl(new String(new char[MAKE_NAME_LEN_MIN - 1]), "model", 100, "category");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenMakeLengthIsAbove15() {
        MotorcycleImpl motorcycle = new MotorcycleImpl(new String(new char[MAKE_NAME_LEN_MAX + 1]), "model", 100, "category");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenModelIsNull() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", null, 100, "category");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenModelLengthIsBelow_1() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", new String(new char[MODEL_NAME_LEN_MIN - 1]), 100, "category");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenModelLengthIsAbove15() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", new String(new char[MODEL_NAME_LEN_MAX + 1]), 100, "category");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenPriceIsNegative() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", -100, "category");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenPriceIsAbove1000000() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 1000001, "category");
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenCategoryIsNull() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 100, null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenCategoryLengthIsBelow_3() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 100, new String(new char[CATEGORY_LEN_MIN - 1]));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenCategoryLengthIsAbove10() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 100, new String(new char[CATEGORY_LEN_MAX + 1]));
    }
    
}
