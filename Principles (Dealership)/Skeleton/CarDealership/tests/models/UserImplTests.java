package models;

import com.telerikacademy.dealership.models.UserImpl;
import com.telerikacademy.dealership.models.common.enums.Role;
import com.telerikacademy.dealership.models.contracts.User;
import org.junit.Test;

//import static com.telerikacademy.dealership.models.common.Constants.UserConstants.*;

public class UserImplTests {


    private static final int USERNAME_LEN_MIN = 2;
    private static final int USERNAME_LEN_MAX = 20;
    private static final int FIRSTNAME_LEN_MIN = 2;
    private static final int FIRSTNAME_LEN_MAX = 20;
    private static final int LASTNAME_LEN_MIN = 2;
    private static final int LASTNAME_LEN_MAX = 20;
    private static final int PASSWORD_LEN_MIN = 5;
    private static final int PASSWORD_LEN_MAX = 30;

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenUsernameIsNull() {
        //Arrange, Act, Assert
        User testUser = new UserImpl(null,
                "firstName",
                "lastName",
                "password",
                Role.NORMAL);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenUsernameIsBelowMinLength() {
        //Arrange, Act, Assert
        User testUser = new UserImpl(new String(new char[USERNAME_LEN_MIN - 1]),
                "firstName",
                "lastName",
                "password",
                Role.NORMAL);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenUsernameIsAboveMaxLength() {
        //Arrange, Act, Assert
        User testUser = new UserImpl(new String(new char[USERNAME_LEN_MAX + 1]),
                "firstName",
                "lastName",
                "password",
                Role.NORMAL);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenUsernameDoesNotMatchPattern() {
        //Arrange, Act, Assert
        User testUser = new UserImpl("U$$ernam3",
                "firstName",
                "lastName",
                "password",
                Role.NORMAL);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenFirstNameIsNull() {
        //Arrange, Act, Assert
        User testUser = new UserImpl("username",
                null,
                "lastName",
                "password",
                Role.NORMAL);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenFirstnameIsBelowMinLength() {
        //Arrange, Act, Assert
        User testUser = new UserImpl("Username",
                new String(new char[FIRSTNAME_LEN_MIN - 1]),
                "lastName",
                "password",
                Role.NORMAL);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenFirstnameIsAboveMaxLength() {
        //Arrange, Act, Assert
        User testUser = new UserImpl("Username",
                new String(new char[FIRSTNAME_LEN_MAX + 1]),
                "lastName",
                "password",
                Role.NORMAL);
    }
    
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenLastNameIsNull() {
        //Arrange, Act, Assert
        User testUser = new UserImpl("username",
                "firstName",
                null,
                "password",
                Role.NORMAL);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenLastNameIsBelowMinLength() {
        //Arrange, Act, Assert
        User testUser = new UserImpl("username",
                "firstName",
                new String(new char[LASTNAME_LEN_MIN - 1]),
                "password",
                Role.NORMAL);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenLastNameIsAboveMaxLength() {
        //Arrange, Act, Assert
        User testUser = new UserImpl("username",
                "firstName",
                new String(new char[LASTNAME_LEN_MAX + 1]),
                "password",
                Role.NORMAL);
    }
    
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenPasswordIsNull() {
        //Arrange, Act, Assert
        User testUser = new UserImpl("username",
                "firstName",
                "lastName",
                null,
                Role.NORMAL);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenPasswordIsBelowMinLength() {
        //Arrange, Act, Assert
        User testUser = new UserImpl("username",
                "firstName",
                "lastName",
                new String(new char[PASSWORD_LEN_MIN - 1]),
                Role.NORMAL);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenPasswordIsAboveMaxLength() {
        //Arrange, Act, Assert
        User testUser = new UserImpl("username",
                "firstName",
                "lastName",
                new String(new char[PASSWORD_LEN_MAX + 1]),
                Role.NORMAL);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenPasswordDoesNotMatchPattern() {
        //Arrange, Act, Assert
        User testUser = new UserImpl("Username",
                "firstName",
                "lastName",
                "Pa-$$_w0rD",
                Role.NORMAL);
    }
    
}
