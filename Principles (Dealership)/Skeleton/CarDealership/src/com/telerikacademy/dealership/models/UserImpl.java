package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.Role;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.User;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class UserImpl implements User {
    private String username, firstName, lastName, password;
    Role role;
    List<Vehicle>vehicles;

    public UserImpl(String username, String firstName, String lastName, String password, Role role, List<Vehicle> vehicles) {
        setUsername(username);
        setFirstName(firstName);
        setLastName(lastName);
        setPassword(password);
        this.role = role;
        this.vehicles = vehicles;
    }
    public UserImpl () {
        System.out.println("Default constructor");
    }
    public UserImpl(String username, String firstName, String lastName, String password, Role role) {
        setUsername(username);
        setFirstName(firstName);
        setLastName(lastName);
        setPassword(password);
        this.role = role;
    }

    private void setUsername(String username) {
        Validator.validateArgumentIsNotNull(username);
        if (username.length() <2 || username.length() > 20) {
            throw new IllegalArgumentException("Username must be between 2 and 20 characters long!");
        }
        this.username = username;
    }

    private void setFirstName(String firstName) {
       Validator.validateArgumentIsNotNull(firstName);
        if (firstName.length() <2 || firstName.length() > 20) {
            throw new IllegalArgumentException("Invalid input!");
        }
        this.firstName = firstName;
    }

    private void setLastName(String lastName) {
        Validator.validateArgumentIsNotNull(lastName);
        if (lastName.length() <2 || lastName.length() > 20) {
            throw new IllegalArgumentException("Invalid input!");
        }
        this.lastName = lastName;
    }

    private void setPassword(String password) {
        Validator.validateArgumentIsNotNull(password);
        if (password.length() < 5 || password.length() > 30) {
            throw new IllegalArgumentException("Password not in range 5 - 30!");
        }
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public Role getRole() {
        return role;
    }

    @Override
    public List<Vehicle> getVehicles() {
        return new ArrayList<>(vehicles);
    }

    @Override
    public void addVehicle(Vehicle vehicle) {
        if
        vehicles.add(vehicle);

    }

    @Override
    public void removeVehicle(Vehicle vehicle) {
    vehicles.remove(vehicle);
    }

    @Override
    public void addComment(Comment commentToAdd, Vehicle vehicleToAddComment) {

    }

    @Override
    public void removeComment(Comment commentToRemove, Vehicle vehicleToRemoveComment) {

    }

    @Override
    public String printVehicles() {
        return null;
    }
}
