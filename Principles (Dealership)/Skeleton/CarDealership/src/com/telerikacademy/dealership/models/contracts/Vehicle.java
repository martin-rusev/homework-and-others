package com.telerikacademy.dealership.models.contracts;

import com.telerikacademy.dealership.models.common.enums.VehicleType;

public interface Vehicle extends Priceable, Commentable {
    
    int getWheels();
    
    VehicleType getType();
    
    String getMakeFromBase();
    
    String getModel();
    
}
