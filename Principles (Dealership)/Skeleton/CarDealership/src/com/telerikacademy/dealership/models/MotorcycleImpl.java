package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

import java.util.List;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {
    private String category;

    public MotorcycleImpl(String make, String model, int wheelsCount, double price, List<Comment> comments, VehicleType vehicleType, String category) {
        super(make, model, wheelsCount, price, comments, vehicleType);
        setCategory(category);
    }

    private void setCategory(String category) {
        Validator.validateArgumentIsNotNull(category);
        Validator.validateCategoryInput(category);
        this.category = category;
    }


    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price);
        this.setCategory(category);
    }


    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public long getMake() {
        return 0;
    }
}

