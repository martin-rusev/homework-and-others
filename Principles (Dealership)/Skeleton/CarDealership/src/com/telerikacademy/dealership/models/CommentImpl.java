package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Commentable;

import java.util.ArrayList;
import java.util.List;

public class CommentImpl implements Comment, Commentable {
    private String content;
    private String author;
    List<Comment>comments;

    public CommentImpl(String content, String author) {
        setContent(content);
        setAuthor(author);
    }
    public CommentImpl() {
        System.out.println("Default Constructor");
    }
    public CommentImpl(String content, String author, List<Comment>comments) {
        setContent(content);
        setAuthor(author);
        setComments(comments);
    }

    public void setComments(List<Comment> comments) {
        Validator.validateArgumentIsNotNull(comments);
        this.comments = comments;
    }

    @Override
    public String getContent() {

        return content;
    }

    private void setContent(String content) {
        Validator.validateArgumentIsNotNull(content);
        Validator.validateContentInput(content);
        this.content = content;
    }

    private void setAuthor(String author) {
        Validator.validateArgumentIsNotNull(author);
        this.author = author;
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }
}
