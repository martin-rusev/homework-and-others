package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class VehicleBase implements Vehicle {
    private String make;
    private String model;
    private int wheelsCount;
    private double price;
    private List<Comment>comments;
    VehicleType vehicleType;

    public VehicleBase(String make, String model, int wheelsCount, double price, List<Comment>comments, VehicleType vehicleType) {
        setMake(make);
        setModel(model);
        setWheelsCount(wheelsCount);
        setPrice(price);
        setComments(comments);
        setVehicleType(vehicleType);
    }
    public VehicleBase () {
        System.out.println("Default contructor");
    }

    public VehicleBase(String make, String model, double price) {
        setMake(make);
        setModel(model);
        setPrice(price);
    }

    private void setMake(String make) {
        Validator.validateArgumentIsNotNull(make);
        Validator.validateModelInput(make);
        this.make = make;
    }

    private void setModel(String model) {
        Validator.validateArgumentIsNotNull(model);
        Validator.validateModelInput(model);
        this.model = model;
    }

    private void setWheelsCount(int wheelsCount) {
        this.wheelsCount = wheelsCount;
    }

    private void setPrice(double price) {
        Validator.checkForPriceArgument(price);
        this.price = price;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    private void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    @Override
    public int getWheels() {
        return wheelsCount;
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    @Override
    public String getMakeFromBase() {
        return make;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public double getPrice() {
        return price;
    }
}
