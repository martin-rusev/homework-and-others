package com.telerikacademy.dealership.models.contracts;

public interface Truck {
    
    int getWeightCapacity();

    long getMake();
}
