package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;
import com.telerikacademy.dealership.models.contracts.Comment;

import java.util.List;

public class CarImpl extends VehicleBase implements Car {
    private int seats;

    public CarImpl(String make, String model, int wheelsCount, double price, List<Comment> comments, VehicleType vehicleType, int seats) {
        super(make, model, wheelsCount, price, comments, vehicleType);
        setSeats(seats);
    }
    public CarImpl() {
        System.out.println("Default constructor");
    }
    public CarImpl(String make, String model, double price,  int seats) {
        super(make, model, price);
        setSeats(seats);
    }


    public void setSeats(int seats) {
        Validator.checkForSeatsArgument(seats);
        this.seats = seats;
    }


    @Override
    public int getSeats() {
        return seats;
    }

    @Override
    public long getMake() {
        return 0;
    }
}


