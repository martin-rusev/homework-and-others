package com.telerikacademy.dealership.models.contracts;

public interface Car{
    
    int getSeats();

    long getMake();
}
