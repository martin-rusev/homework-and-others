package com.telerikacademy.dealership.models.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    public static final String NULL_ARGUMENT_ERROR = "Argument cannot be null!";
    public static final int MAKE_MODEL_MIN_LENGTH = 2;
    public static final int MAKE_MODEL_MAX_LENGTH = 10;
    public static final int SEATS_MIN_LENGTH = 1;
    public static final int SEATS_MAX_LENGTH = 10;
    public static final int CATEGORY_MIN_LENGTH = 3;
    public static final int CATEGORY_MAX_LENGTH = 10;
    public static final int CAPACITY_MAX_LENGTH = 100;
    public static final int CONTENT_MAX_LENGTH = 200;


    public static final String MAKE_MODEL_ERROR_FORMAT = "The input must be %d - %d characters long!";
    public static final String PRICE_ARGUMENT_ERROR = "Argument cannot be negative or more than 1000000!";
    public static final String SEATS_ARGUMENT_ERROR = "Argument must be in range %d - %d";

    public static void validateIntRange(int value, int min, int max, String message) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(message);
        }
    }
    
    public static void validateDecimalRange(double value, double min, double max, String message) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(message);
        }
    }
    
    public static void validatePattern(String value, String pattern) {
        Pattern patternToMatch = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        Matcher matcher = patternToMatch.matcher(value);
        
        if (!matcher.matches()) // returns !true only if the whole value matches the pattern
        //if (m.find()) // if we want to find if there is substring of value that matches the pattern
        {
            throw new IllegalArgumentException();
        }
    }
    
    public static void validateArgumentIsNotNull(Object arg) {
        if (arg == null) {
            throw new IllegalArgumentException(NULL_ARGUMENT_ERROR);
        }
    }
    private static void faceControl(String face, int min, int max, String error) {
        validateArgumentIsNotNull(face);
        if (face.length() < min || face.length() > max) {
            throw new IllegalArgumentException(String.format(error, min, max));
        }
    }
    public static void validateMakeInput(String make) {
        faceControl(make, MAKE_MODEL_MIN_LENGTH, MAKE_MODEL_MAX_LENGTH, MAKE_MODEL_ERROR_FORMAT);
    }
    public static void validateModelInput(String model) {
        faceControl(model, MAKE_MODEL_MIN_LENGTH, MAKE_MODEL_MAX_LENGTH, MAKE_MODEL_ERROR_FORMAT);
    }
    public static void checkForPriceArgument(double price) {
        if (price < 0 || price > 1000000) {
            throw new IllegalArgumentException(PRICE_ARGUMENT_ERROR);
        }
    }
    public static void checkForSeatsArgument(int seats) {
        if (seats < 1 || seats > 10) {
            throw new IllegalArgumentException(String.format(SEATS_ARGUMENT_ERROR, SEATS_MIN_LENGTH, SEATS_MAX_LENGTH));
        }
    }
    public static void validateCategoryInput(String category) {
        faceControl(category, CATEGORY_MIN_LENGTH, CATEGORY_MAX_LENGTH, MAKE_MODEL_ERROR_FORMAT);
    }
    public static void checkForWeightCapacity(int capacity) {
        if (capacity < 1 || capacity > 100) {
            throw new IllegalArgumentException(String.format(SEATS_ARGUMENT_ERROR, SEATS_MIN_LENGTH, CAPACITY_MAX_LENGTH));
        }
    }
    public static void validateContentInput(String content) {
        faceControl(content, CATEGORY_MIN_LENGTH, CONTENT_MAX_LENGTH, SEATS_ARGUMENT_ERROR);
    }




}
