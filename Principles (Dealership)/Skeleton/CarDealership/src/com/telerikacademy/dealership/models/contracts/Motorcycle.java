package com.telerikacademy.dealership.models.contracts;

public interface Motorcycle {
    
    String getCategory();

    long getMake();
}
