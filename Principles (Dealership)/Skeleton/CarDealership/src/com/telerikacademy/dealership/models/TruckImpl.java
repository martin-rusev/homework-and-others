package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Truck;

import java.util.List;

public class TruckImpl extends VehicleBase implements Truck {
    private int weightCapacity;

    public TruckImpl(String make, String model, int wheelsCount, double price, List<Comment> comments, VehicleType vehicleType, int weightCapacity) {
        super(make, model, wheelsCount, price, comments, vehicleType);
        setWeightCapacity(weightCapacity);
    }
    public TruckImpl () {
        System.out.println("Default Constructor");
    }
    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price);
        setWeightCapacity(weightCapacity);
    }


    public void setWeightCapacity(int weightCapacity) {
        Validator.checkForWeightCapacity(weightCapacity);
        this.weightCapacity = weightCapacity;
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

    @Override
    public long getMake() {
        return 0;
    }
}
