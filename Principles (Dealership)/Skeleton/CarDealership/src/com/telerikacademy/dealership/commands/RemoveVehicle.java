package com.telerikacademy.dealership.commands;

import com.telerikacademy.dealership.commands.contracts.Command;
import com.telerikacademy.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.contracts.User;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.List;

import static com.telerikacademy.dealership.commands.constants.CommandConstants.REMOVED_VEHICLE_DOES_NOT_EXIST;
import static com.telerikacademy.dealership.commands.constants.CommandConstants.VEHICLE_REMOVED_SUCCESSFULLY;

public class RemoveVehicle implements Command {
    
    private final DealershipFactory dealershipFactory;
    private final DealershipRepository dealershipRepository;
    
    
    public RemoveVehicle(DealershipFactory dealershipFactory, DealershipRepository dealershipRepository) {
        this.dealershipFactory = dealershipFactory;
        this.dealershipRepository = dealershipRepository;
    }
    
    @Override
    public String execute(List<String> parameters) {
        int vehicleIndex = Integer.parseInt(parameters.get(0)) - 1;
        return removeVehicle(vehicleIndex);
    }
    
    private String removeVehicle(int vehicleIndex) {
        User loggedUser = dealershipRepository.getLoggedUser();
        Validator.validateIntRange(vehicleIndex, 0, loggedUser.getVehicles().size(), REMOVED_VEHICLE_DOES_NOT_EXIST);
        
        Vehicle vehicle = loggedUser.getVehicles().get(vehicleIndex);
        
        loggedUser.removeVehicle(vehicle);
        
        return String.format(VEHICLE_REMOVED_SUCCESSFULLY, loggedUser.getUsername());
    }
    
}
