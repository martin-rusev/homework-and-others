package com.telerikacademy.dealership.commands.enums;

public enum CommandType {
    REGISTERUSER,
    LOGIN,
    LOGOUT,
    ADDVEHICLE,
    REMOVEVEHICLE,
    ADDCOMMENT,
    REMOVECOMMENT,
    SHOWUSERS,
    SHOWVEHICLES
}
