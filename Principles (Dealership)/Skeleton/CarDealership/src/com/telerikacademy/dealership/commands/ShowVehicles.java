package com.telerikacademy.dealership.commands;

import com.telerikacademy.dealership.commands.contracts.Command;
import com.telerikacademy.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.dealership.models.contracts.User;

import java.util.List;

import static com.telerikacademy.dealership.commands.constants.CommandConstants.NO_SUCH_USER;

public class ShowVehicles implements Command {
    
    private final DealershipFactory dealershipFactory;
    private final DealershipRepository dealershipRepository;
    
    
    public ShowVehicles(DealershipFactory dealershipFactory, DealershipRepository dealershipRepository) {
        this.dealershipFactory = dealershipFactory;
        this.dealershipRepository = dealershipRepository;
    }
    
    @Override
    public String execute(List<String> parameters) {
        String username = parameters.get(0);
        
        return showUserVehicles(username);
    }
    
    private String showUserVehicles(String username) {
        User user = dealershipRepository.getUsers().stream().filter(u -> u.getUsername().toLowerCase().equals(username.toLowerCase())).findFirst().orElse(null);
        
        if (user == null) {
            return String.format(NO_SUCH_USER, username);
        }
        
        return user.printVehicles();
    }
    
}
