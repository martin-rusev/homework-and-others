package com.telerikacademy.dealership.commands.constants;

public class CommandConstants {
    
    public final static String INVALID_COMMAND = "Invalid command! %s";
    
    public final static String USER_ALREADY_EXIST = "User %s already exist. Choose a different username!";
    public final static String USER_LOGGED_IN_ALREADY = "User %s is logged in! Please log out first!";
    public final static String USER_REGISTERED = "User %s registered successfully!";
    public final static String NO_SUCH_USER = "There is no user with username %s!";
    public final static String USER_LOGGED_OUT = "You logged out!";
    public final static String USER_LOGGED_IN = "User %s successfully logged in!";
    public final static String WRONG_USERNAME_OR_PASSWORD = "Wrong username or password!";
    public final static String YOU_ARE_NOT_AN_ADMIN = "You are not an admin!";
    
    public final static String COMMENT_ADDED_SUCCESSFULLY = "%s added comment successfully!";
    public final static String COMMENT_REMOVED_SUCCESSFULLY = "%s removed comment successfully!";
    
    public final static String VEHICLE_REMOVED_SUCCESSFULLY = "%s removed vehicle successfully!";
    public final static String VEHICLE_ADDED_SUCCESSFULLY = "%s added vehicle successfully!";
    
    public final static String REMOVED_VEHICLE_DOES_NOT_EXIST = "Cannot remove comment! The vehicle does not exist!";
    public final static String REMOVED_COMMENT_DOES_NOT_EXIST = "Cannot remove comment! The comment does not exist!";
    
    public final static String COMMENT_DOES_NOT_EXIST = "The comment does not exist!";
    public final static String VEHICLE_DOES_NOT_EXIST = "The vehicle does not exist!";
    public static final String REPORT_SEPARATOR = "####################";
    public static final int NUMBER_OF_PARAMETERS_FOR_NORMAL_ROLE = 4;
    
}
