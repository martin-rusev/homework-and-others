package com.telerikacademy.dealership.commands;

import com.telerikacademy.dealership.commands.contracts.Command;
import com.telerikacademy.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.dealership.core.contracts.DealershipRepository;
import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.User;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.List;

import static com.telerikacademy.dealership.commands.constants.CommandConstants.*;

public class AddComment implements Command {
    
    private final DealershipFactory dealershipFactory;
    private final DealershipRepository dealershipRepository;
    
    public AddComment(DealershipFactory dealershipFactory, DealershipRepository dealershipRepository) {
        this.dealershipFactory = dealershipFactory;
        this.dealershipRepository = dealershipRepository;
    }
    
    @Override
    public String execute(List<String> parameters) {
        String content = parameters.get(0);
        String author = parameters.get(1);
        int vehicleIndex = Integer.parseInt(parameters.get(2)) - 1;
        
        return addComment(content, vehicleIndex, author);
    }
    
    private String addComment(String content, int vehicleIndex, String author) {
        Comment comment = dealershipFactory.createComment(content, dealershipRepository.getLoggedUser().getUsername());
        User user = dealershipRepository.getUsers().stream().filter(u -> u.getUsername().toLowerCase().equals(author.toLowerCase())).findFirst().orElse(null);
        
        if (user == null) {
            return String.format(NO_SUCH_USER, author);
        }
        
        Validator.validateIntRange(vehicleIndex, 0, user.getVehicles().size(), VEHICLE_DOES_NOT_EXIST);
        
        Vehicle vehicle = user.getVehicles().get(vehicleIndex);
        
        dealershipRepository.getLoggedUser().addComment(comment, vehicle);
        
        return String.format(COMMENT_ADDED_SUCCESSFULLY, dealershipRepository.getLoggedUser().getUsername());
    }
    
}
