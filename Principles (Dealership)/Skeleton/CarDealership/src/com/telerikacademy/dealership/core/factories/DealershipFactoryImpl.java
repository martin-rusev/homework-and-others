package com.telerikacademy.dealership.core.factories;

import com.telerikacademy.dealership.core.contracts.DealershipFactory;
import com.telerikacademy.dealership.models.*;
import com.telerikacademy.dealership.models.common.enums.Role;
import com.telerikacademy.dealership.models.contracts.*;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class DealershipFactoryImpl implements DealershipFactory {
    
    public Car createCar(String make, String model, double price, int seats) {
        // TODO: Implement this
        return new CarImpl(make, model,price,seats);
    }
    
    public Motorcycle createMotorcycle(String make, String model, double price, String category) {
        // TODO: Implement this
       // throw new NotImplementedException();
        return new MotorcycleImpl(make, model, price,category);
    }
    
    public Truck createTruck(String make, String model, double price, int weightCapacity) {
        // TODO: Implement this
        return new TruckImpl(make, model, price, weightCapacity);

    }
    
    public User createUser(String username, String firstName, String lastName, String password, String role) {
        // TODO: Implement this
        return new UserImpl(username, firstName, lastName,password, Role.valueOf(role.toUpperCase()));
    }
    
    public Comment createComment(String content, String author) {
        // TODO: Implement this
        return new CommentImpl(content,author);
    }
    
}
