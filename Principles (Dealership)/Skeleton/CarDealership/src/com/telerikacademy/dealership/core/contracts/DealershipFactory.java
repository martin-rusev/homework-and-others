package com.telerikacademy.dealership.core.contracts;

import com.telerikacademy.dealership.models.contracts.*;

public interface DealershipFactory {
    
    User createUser(String username, String firstName, String lastName, String password, String role);
    
    Comment createComment(String content, String author);
    
    Car createCar(String make, String model, double price, int seats);
    
    Motorcycle createMotorcycle(String make, String model, double price, String category);
    
    Truck createTruck(String make, String model, double price, int weightCapacity);
    
}
